#ifndef SHIFT_HPP
#define SHIFT_HPP

SC_MODULE(Shift) {
	static const unsigned DATA_WIDTH = 16;
	static const unsigned DATA_WIDTH_IN = 10;
		
	sc_in<sc_lv<DATA_WIDTH_IN> > din;
	sc_in<unsigned> val;
	sc_out<sc_lv<DATA_WIDTH> > dout;
	
	SC_CTOR(Shift) {
		SC_THREAD(update_ports);
			sensitive << din << val;
	}
	
	private:
	void update_ports();
};

#endif
