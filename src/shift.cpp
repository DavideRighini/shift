#include <systemc.h>
#include "shift.hpp"

void Shift::update_ports() {
	while(true) {
		wait();	
		dout->write(din->read().to_int() << val->read());	
	}
}
