#include <systemc.h>
#include "shift.hpp"

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

static const unsigned TEST_SIZE = 6;
static const unsigned DATA_WIDTH = 16;
sc_fifo<int> observer_shift;
int values1[TEST_SIZE]; //vettore usato per tutti i test
int values2[TEST_SIZE]; //vettore usato per tutti i test

SC_MODULE(TestBench) 
{
 	public:
	sc_signal<sc_lv<10> > shift_din;
	sc_signal<unsigned> shift_val;
	sc_signal<sc_lv<DATA_WIDTH> > shift_dout; 

	Shift shift1; //SHIFT VECTOR

  SC_CTOR(TestBench) : shift1("shift1")
  {
 		init_values();

		SC_THREAD(init_values_shift_test);
		shift1.din(this->shift_din);
		shift1.dout(this->shift_dout);
		shift1.val(this->shift_val);		
		SC_THREAD(observer_thread_shift);
			sensitive << shift1.dout;

  }

	void init_values() {
				values1[0] = 5;
				values1[1] = 4;
				values1[2] = 3;
				values1[3] = 2;
				values1[4] = 1;
				values1[5] = 0;
				
				values2[0] = 1;
				values2[1] = 3;
				values2[2] = 5;
				values2[3] = 7;
				values2[4] = 9;
				values2[5] = 0;
						  		  
	}

	void init_values_shift_test() {
		shift_val.write(6); //devo shiftare il vettore di 6 posti
		for (unsigned i=0;i<TEST_SIZE;i++) {
		  shift_din.write(values1[i]);
		  wait(10,SC_NS);
		}
		wait(50,SC_NS);
		sc_stop();	  
	}

	void observer_thread_shift() {
		while(true) {
				wait();
				int value = shift1.dout->read().to_int();
				cout << "observer_thread: at " << sc_time_stamp() << " shift out: " << shift1.dout->read() << endl;
				if (observer_shift.num_free()>0 ) observer_shift.write(value);
		} 
	}

	int check_shift() {
		int test=0;
		for (unsigned i=0;i<TEST_SIZE;i++) {
				test=observer_shift.read(); //porta fifo
		    if (test != (values1[i] << 6 ) )
		        return 1;
		}
		return 0;
	}

};

// ---------------------------------------------------------------------------------------

int sc_main(int argc, char* argv[]) {

	TestBench testbench("testbench");
	sc_start();

  return testbench.check_shift();
}
